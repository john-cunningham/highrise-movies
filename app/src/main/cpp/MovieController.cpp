//
//  MovieController.hpp
//  Highrise
//
//  Created by Jimmy Xu on 12/19/18.
//  Copyright © 2019 Highrise. All rights reserved.
//

#ifndef MovieController_hpp
#define MovieController_hpp

#include <string>
#include <vector>
#include <map>
#include <jni.h>

namespace movies {

    class Actor {
    public:
        std::string name;
        int age;

        //optional challenge 1: Load image from URL
        std::string imageUrl;
    };

    class Movie {
    public:
        std::string name;
        int lastUpdated;

    };

    class MovieDetail {
    public:
        std::string name;
        float score;
        std::vector<Actor> actors;
        std::string description;
    };

    class MovieController {
    private:
        std::vector<Movie *> _movies;
        std::map<std::string, MovieDetail *> _details;

    public:
        MovieController() {
            //populate data
            for (int i = 0; i < 10; i++) {
                auto movie = new Movie();
                movie->name = "Top Gun " + std::to_string(i);
                movie->lastUpdated = i * 10000;
                _movies.push_back(movie);

                auto movieDetail = new MovieDetail();
                movieDetail->name = movie->name;
                movieDetail->score = rand() % 10;
                movieDetail->description = "As students at the United States Navy's elite fighter weapons school compete to be best in the class, one daring young pilot learns a few things from a civilian instructor that are not taught in the classroom.";

                auto tomCruise = Actor();
                tomCruise.name = "Tom Cruise";
                tomCruise.age = 50;

                auto valKilmer = Actor();
                valKilmer.name = "Val Kilmer";
                valKilmer.age = 46;
                valKilmer.imageUrl = "https://m.media-amazon.com/images/M/MV5BMTk3ODIzMDA5Ml5BMl5BanBnXkFtZTcwNDY0NTU4Ng@@._V1_UY317_CR4,0,214,317_AL_.jpg";

                movieDetail->actors.push_back(tomCruise);
                movieDetail->actors.push_back(valKilmer);

                if (i % 2 == 0) {
                    auto timRobbins = Actor();
                    timRobbins.name = "Tim Robbins";
                    timRobbins.age = 55;
                    timRobbins.imageUrl = "https://m.media-amazon.com/images/M/MV5BMTI1OTYxNzAxOF5BMl5BanBnXkFtZTYwNTE5ODI4._V1_UY317_CR16,0,214,317_AL_.jpg";

                    movieDetail->actors.push_back(timRobbins);
                } else {
                    auto jenniferConnelly = Actor();
                    jenniferConnelly.name = "Jennifer Connelly";
                    jenniferConnelly.age = 39;
                    jenniferConnelly.imageUrl = "https://m.media-amazon.com/images/M/MV5BOTczNTgzODYyMF5BMl5BanBnXkFtZTcwNjk4ODk4Mw@@._V1_UY317_CR12,0,214,317_AL_.jpg";

                    movieDetail->actors.push_back(jenniferConnelly);
                }

                _details[movie->name] = movieDetail;
            }
        }

        //Returns list of movies
        std::vector<Movie *> getMovies() {
            return _movies;
        }

        //Returns details about a specific movie
        MovieDetail *getMovieDetail(std::string name) {
            for (auto item:_details) {
                if (item.second->name == name) {
                    return item.second;
                }
            }
            return nullptr;
        }
    };


    /**
     * Get Movies
     * Accessible from Android
     * @return jobjectArray of Movie objects
     */
    extern "C" JNIEXPORT jobjectArray JNICALL
    Java_com_highrise_movies_MainActivity_getMovies(
            JNIEnv* env,
            jobject /* this */) {

        auto mc = new MovieController();
        auto movies = mc->getMovies();

        // Find Movie class and create returned array
        jclass movieClass = env->FindClass("com/highrise/movies/Movie");
        jobjectArray movieArray = env->NewObjectArray(static_cast<jsize>(movies.size()), movieClass, 0 );

        // Iterate over movies...
        for (int i = 0; i < movies.size(); i++) {

            // Instantiate class
            jmethodID methodId = env->GetMethodID(movieClass, "<init>", "()V");
            jobject movie = env->NewObject(movieClass, methodId, nullptr);

            // Get field IDs
            jfieldID movieNameFieldId = env->GetFieldID(movieClass, "name", "Ljava/lang/String;");
            jfieldID lastUpdatedFieldId = env->GetFieldID(movieClass, "lastUpdated", "I");

            // Set field values
            env->SetObjectField(movie, movieNameFieldId, env->NewStringUTF(movies[i]->name.c_str()));
            env->SetIntField(movie, lastUpdatedFieldId, movies[i]->lastUpdated);

            // Add movie to returned array
            env->SetObjectArrayElement(movieArray, i, movie);
        }

        return movieArray;
    }


    /**
     * Get Movie Details
     * Accessible from Android
     * @return jobject of MovieDetail
     */
    extern "C" JNIEXPORT jobject JNICALL
    Java_com_highrise_movies_MainActivity_getMovieDetails(
            JNIEnv* env,
            jobject  /* this */,
            jstring name) {

        if (name == nullptr) {
            return nullptr;
        }

        const char *movieName= env->GetStringUTFChars(name, 0);
        //std::string str(movieName);




        auto mc = new MovieController();
        auto movieDetails = mc->getMovieDetail(movieName);
        //need to release this string when done with it in order to
        //avoid memory leak
        env->ReleaseStringUTFChars(name, movieName);

        if (movieDetails != nullptr)
        {
            // Find MovieDetail class and create returned array
            jclass movieDetailClass = env->FindClass("com/highrise/movies/MovieDetail");
            jmethodID methodId = env->GetMethodID(movieDetailClass, "<init>", "()V");
            jobject movieDetailsReturnObject = env->NewObject(movieDetailClass, methodId, nullptr);

            // Get field IDs
            jfieldID movieNameFieldId = env->GetFieldID(movieDetailClass, "name", "Ljava/lang/String;");
            jfieldID movieScoreFieldId = env->GetFieldID(movieDetailClass, "score", "F");
            jfieldID movieActorsFieldId = env->GetFieldID(movieDetailClass, "actors", "Ljava/util/ArrayList;"); // java.util.ArrayList<E>
            jfieldID movieDescriptionFieldId = env->GetFieldID(movieDetailClass, "description", "Ljava/lang/String;");

            // Set field values
            env->SetObjectField(movieDetailsReturnObject, movieNameFieldId, env->NewStringUTF(movieDetails->name.c_str()));
            env->SetFloatField(movieDetailsReturnObject, movieScoreFieldId, (jfloat)movieDetails->score);
            env->SetObjectField(movieDetailsReturnObject, movieDescriptionFieldId, env->NewStringUTF(movieDetails->description.c_str()));

            if (movieDetails->actors.size() > 0)
            {
                jclass java_util_ArrayList = (jclass)env->NewGlobalRef(env->FindClass("java/util/ArrayList"));
                jmethodID java_util_ArrayList_     = env->GetMethodID(java_util_ArrayList, "<init>", "(I)V");
                jmethodID java_util_ArrayList_add = env->GetMethodID(java_util_ArrayList, "add", "(Ljava/lang/Object;)Z");
                jobject result = env->NewObject(java_util_ArrayList, java_util_ArrayList_, movieDetails->actors.size());

                for (int i = 0; i < movieDetails->actors.size(); i++)
                {
                    auto actor = movieDetails->actors[i];
                    jclass actorClass = env->FindClass("com/highrise/movies/Actor");
                    jmethodID actorMethodId = env->GetMethodID(actorClass, "<init>", "()V");
                    jobject actorObject = env->NewObject(actorClass, actorMethodId, nullptr);


                    jfieldID actorNameFieldId = env->GetFieldID(actorClass, "name", "Ljava/lang/String;");
                    jfieldID actorAgeFieldId = env->GetFieldID(actorClass, "age", "I");
                    jfieldID actorImageUrlFieldId = env->GetFieldID(actorClass, "imageUrl", "Ljava/lang/String;");


                    env->SetObjectField(actorObject, actorNameFieldId, env->NewStringUTF(actor.name.c_str()));
                    env->SetIntField(actorObject, actorAgeFieldId, actor.age);
                    env->SetObjectField(actorObject, actorImageUrlFieldId, env->NewStringUTF(actor.imageUrl.c_str()));


                    env->CallBooleanMethod(result, java_util_ArrayList_add, actorObject);
                    env->DeleteLocalRef(actorObject);
                }

                env->SetObjectField(movieDetailsReturnObject, movieActorsFieldId, result);
            }

            return movieDetailsReturnObject;
        }
        else {
            return nullptr;
        }
    }
}

#endif /* MovieController_hpp */
