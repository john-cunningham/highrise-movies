package com.highrise.movies

class Actor {
    var name: String? = null
    var age: Int = 0
    var imageUrl: String? = null
}