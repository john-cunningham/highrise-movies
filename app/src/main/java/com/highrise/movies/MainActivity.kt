package com.highrise.movies

import android.support.v7.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, MainFragment())
                .commitNow()
        }
    }

    /**
     * A native method that is implemented by the 'MovieController' native library,
     * which is packaged with this application.
     */
    external fun getMovies(): Array<Movie>
    external fun getMovieDetails(movieName:String): MovieDetail

    companion object {

        // Used to load the 'MovieController' library on application startup.
        init {
            System.loadLibrary("movie-controller")
        }
    }
}
