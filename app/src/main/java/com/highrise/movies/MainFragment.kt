package com.highrise.movies

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.main_fragment.*

class MainFragment : Fragment() {

    lateinit var movies:Array<Movie>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val activity = activity as MainActivity
        movies = activity.getMovies()

        movieList.layoutManager = LinearLayoutManager(activity.applicationContext)
        movieList.itemAnimator = DefaultItemAnimator()
        movieList.adapter = MovieAdapter(movies)

        movieList.addOnItemTouchListener(
            RecyclerItemTouchListener(activity,
                object : RecyclerItemTouchListener.OnItemClickListener {
                    override fun onItemClick(position: Int) {

                        val movieTitle = movies[position].name

                        val newFragment = MovieDetailsFragment.newInstance(movieTitle)
                        val transaction = activity.supportFragmentManager.beginTransaction()
                        transaction.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out, android.R.animator.fade_in, android.R.animator.fade_out)
                        transaction.replace(R.id.container, newFragment)
                        transaction.addToBackStack(null)
                        transaction.commit()
                    }
            })
        )
    }
}
