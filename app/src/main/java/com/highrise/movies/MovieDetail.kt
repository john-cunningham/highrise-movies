package com.highrise.movies

class MovieDetail {
    var name: String? = null
    var score: Float = 0f
    var actors: ArrayList<Actor>? = null
    var description: String? = null
}
