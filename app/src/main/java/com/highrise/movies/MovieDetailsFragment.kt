package com.highrise.movies

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.movie_details_fragment.*
import kotlinx.android.synthetic.main.movie_details_actor.view.*
import com.squareup.picasso.Picasso

class MovieDetailsFragment : Fragment() {

    lateinit var movie:MovieDetail
    lateinit var movieTitle:String

    companion object {
        fun newInstance(movieTitle: String?): MovieDetailsFragment {
            val fragment = MovieDetailsFragment()
            val args = Bundle()
            args.putString("movieTitle", movieTitle)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View
    {
        return inflater.inflate(R.layout.movie_details_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val activity = activity as MainActivity
        movieTitle = arguments!!.getString("movieTitle", null)
        movie = activity.getMovieDetails(movieTitle)

        // Title
        movieDetailsTitle.text = movie.name

        // Rating
        movieDetailsRating.rating = movie.score

        // Description
        movieDetailsDescription.text = movie.description

        // Actors
        val actorsLayout = movieDetailsActors
        val inflater = LayoutInflater.from(activity)

        for (actor in movie.actors!!) {
            val actorView = inflater.inflate(R.layout.movie_details_actor, null)
            actorView.actor_name.text = "${actor.name} (${actor.age})"
            if (!actor.imageUrl.isNullOrEmpty()) {
                Picasso.get().load(actor.imageUrl).into(actorView.actor_image)
            } else {
                actorView.actor_image.setImageResource(R.drawable.default_actor)
            }

            actorsLayout.addView(actorView)
        }
    }
}
