/**
 * onClickListener solution for RecyclerView
 * Borrowed from https://www.androidfizz.com/android-recyclerview-onitemclicklistener/
 */
package com.highrise.movies

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View

class RecyclerItemTouchListener(context: Context, private val listener: OnItemClickListener) :
    RecyclerView.OnItemTouchListener {
    private val gestureDetector: GestureDetector

    private var childView: View? = null

    private var childViewPosition: Int = 0

    init {
        this.gestureDetector = GestureDetector(context, GestureListener())
    }

    override fun onInterceptTouchEvent(recyclerView: RecyclerView, event: MotionEvent): Boolean {
        childView = recyclerView.findChildViewUnder(event.x, event.y)
        if (childView != null) {
            childViewPosition = recyclerView.getChildAdapterPosition(childView!!)
        }
        return childView != null && gestureDetector.onTouchEvent(event)
    }

    override fun onTouchEvent(view: RecyclerView, event: MotionEvent) {
        // Not needed.
    }

    override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {

    }

    /**
     * A click listener for items.
     */
    interface OnItemClickListener {
        fun onItemClick(position: Int)
//        fun onItemLongPress(position: Int)
    }

    protected inner class GestureListener : GestureDetector.SimpleOnGestureListener() {

        override fun onSingleTapUp(event: MotionEvent): Boolean {
            if (childView != null) {
                listener.onItemClick(childViewPosition)
            }
            return true
        }

//        override fun onLongPress(event: MotionEvent) {
//            if (childView != null) {
//                listener.onItemLongPress(childViewPosition)
//            }
//        }

        override fun onDown(event: MotionEvent): Boolean {
            // Best practice to always return true here.
            // http://developer.android.com/training/gestures/detector.html#detect
            return true
        }
    }
}